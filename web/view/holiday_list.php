<?php include './view/header.php'; ?>
<main>
      <h1>Holiday List</h1>
      <link href="mian.css" rel="stylesheet" type="text/css"/>
    <a href="holiday_add"></a>
  
    <section>
        <table>
            <tr>
                <th>Holidays_id</th>
                <th>Description</th>
                <th>Date_Start</th>
                <th>Date_End</th>
                <th>Destination</th>
                <th class="right">Cost</th>



                          <th>Averages Difference</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>

            </tr>
               <?php $averageCount = 0; $totalValue = 0; ?>
                    <?php foreach ($holidays as $holiday) : 
            $averageCount = $averageCount + 1; 
            $totalValue = $totalValue + $holiday['cost']; ?>
            <?php endforeach; 
            $average = round($totalValue / $averageCount,2);?>

            <?php foreach ($holidays as $holiday) : ?>
            <tr>
                <td><?php echo $holiday['holidays_id']; ?></td>
                        <td><?php echo $holiday['description']; ?></td>
                        <td><?php echo $holiday['date_start']; ?></td>
                        <td><?php echo $holiday['date_end']; ?></td>
                        <td><?php echo $holiday['destination']; ?></td>
                        <td><?php echo $holiday['cost']; ?></td>
                        <td class="right"><?php echo abs($holiday['cost'] - $average); ?></td>
                
           
                                <td><form action="." method="post">
                                    <input type="hidden" name="action" value="delete_holiday">
                                        <input type="hidden" name="holidays_id" value="<?php echo $holiday['holidays_id']; ?>">
                                        <input type="submit" value="Delete">
                            </form></td>

            </tr>
             <?php endforeach; ?>


             <tr>
                <td>&nbsp;</td>
                <td><b>Total Average Price:</b></td>
                <td class="right"><b><?php echo $average ?></b></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <th>&nbsp;</th>
            </tr>   
        </table>
       <a href="?action=show_add_form">Add Holiday</a>
    </section>
</mian>
<?php include './view/footer.php'; ?>
<!DOCTYPE html>
<html>
<head>
<title>File Uploading With My Holiday Memory</title>
</head>
<body>
<div id="header">
<label>File Uploading With My Holiday Memory</label>
</div>
<div id="body">
    <form action="upload.php" method="post" enctype="multipart/form-data">
    <input type="file" name="file" />
    <button type="submit" name="btn-upload">upload</button>
    </form>
    <br />
    <?php
    if(isset($_GET['success']))
    {
        ?>
        <label>File Uploaded Successfully...  <a href="view_image.php">click here to view file.</a></label>
        <?php
    }
    else if(isset($_GET['fail']))
    {
        ?>
        <label>Problem While File Uploading !</label>
        <?php
    }
    else
    {
        ?>
        <label>Try to upload any files(PDF, DOC, EXE, VIDEO, MP3, ZIP,etc...)</label>
        <?php
    }
    ?>
</div>
</body>
</html>


