<?php
require('database.php') ;
$query = 'SELECT * FROM holidays';
$statement = $db->prepare($query);
$statement->execute();
$holidays = $statement->fetchAll();
$statement->closeCursor();
?>



<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>My Holiday Memory</title>
        <link href="mian.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <header><h1>Holiday Manage</h1></header>
        <main>
            <h1>Add Holiday</h1>
            <form action="holiday_add.php" method="post" id="add_holiday_form">
                <label>Holidays_id:</label>
                <input type="text" name="holidays_id"><br>
                <label>Description:</label>
                <input type="text" name="description"><br>
                <label>Date_Start:</label>
                <input type="text" name="date_start"><br>
                <label>Date_End:</label>
                <input type="text" name="date_end">
                <label>Destination:</label>
                <input type="text" name="destination">
                <label>Cost:</label>
                <input type="text" name="cost">
                
                <label>&nbsp;</label>
                <input type="submit" value="Add Holiday"><br>
            </form>
            <p><a href="index.php">View Holiday List</a></p>
        </main>
        <footer>
            <p>&copy; <?php echo date("Y"); ?> My Holiday Memory, Inc.</p>
        </footer>
    </body>
</html>
